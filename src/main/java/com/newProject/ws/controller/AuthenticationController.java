package com.newProject.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newProject.ws.model.User;
import com.newProject.ws.service.AuthenticationService;
import com.newProject.ws.service.UserService;

//son olarak authentication endpoint oluşturmaca 8.

@RestController
@RequestMapping("api/authentication")
public class AuthenticationController {
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired
	private UserService userService;
	
	@PostMapping("sign-up")
	public ResponseEntity<?> signup(@RequestBody User user)    //@RequestPAram uç noktalara ulaşmak için :)))
	{
		if (userService.findByUsername(user.getUsername()).isPresent())
		{
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(userService.saveUser(user), HttpStatus.CREATED);
	}
	
	@PostMapping("sign-in")
	public ResponseEntity<?> signIn(@RequestBody User user)
	{
		return new ResponseEntity<>(authenticationService.signInAndReturnJWT(user),HttpStatus.OK);
	}
}
