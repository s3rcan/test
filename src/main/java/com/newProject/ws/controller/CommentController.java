package com.newProject.ws.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newProject.ws.model.Comment;
import com.newProject.ws.model.Player;
import com.newProject.ws.service.CommentService;
import com.newProject.ws.shared.GenericResponse;

@RestController
@RequestMapping("/comments")
public class CommentController {
	
	@Autowired
	CommentService commentService;
	
	@PostMapping("/")
	GenericResponse saveComment(@Valid @RequestBody Comment comment, Player player) {
		commentService.save(comment, player);
		return new GenericResponse("Comment is saved!");
	}
	
	@DeleteMapping("/comments/{id}")
	GenericResponse deleteComments(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new GenericResponse("Comment Removed!");
	}
}
