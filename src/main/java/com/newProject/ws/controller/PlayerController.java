package com.newProject.ws.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newProject.ws.model.Player;
import com.newProject.ws.model.vm.PlayerUpdateVM;
import com.newProject.ws.model.vm.PlayerVM;
import com.newProject.ws.service.PlayerService;
import com.newProject.ws.shared.GenericResponse;

@RestController
@RequestMapping("/players")
public class PlayerController {
	
	@Autowired
	PlayerService playerService;

	
	@PostMapping("/")
	GenericResponse createPlayer(@RequestBody Player player) {
		playerService.save(player);
		return new GenericResponse("Player Created :) ");
	}
	
	@DeleteMapping("/{id}")	
	GenericResponse deletePlayer(@PathVariable Integer id) {
		playerService.deletePlayer(id);
		return new GenericResponse("Player Removed :( ");
	}
	
	@PutMapping("/{firstName}")
	PlayerVM updatePlayer(@Valid @RequestBody PlayerUpdateVM updatedPlayer, @PathVariable String firstName) {
	Player player = playerService.updatePlayer(firstName, updatedPlayer);
	return new PlayerVM(player);
	}
}
