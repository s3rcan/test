package com.newProject.ws.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.newProject.ws.model.User;
import com.newProject.ws.repository.UserRepository;

@Service
public class UserService {
	
	UserRepository userRepository;
	
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public User saveUser(User user) {	
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setCreateTime(LocalDateTime.now());
		return userRepository.save(user);
	}
	
	public Optional<User> findByUsername(String username) { 
		
		return userRepository.findByUsername(username);
	}
	
	public List<User> findAllUsers() {
		return userRepository.findAll();
	}
}
