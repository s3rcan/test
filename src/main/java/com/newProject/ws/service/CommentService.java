package com.newProject.ws.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.newProject.ws.model.Comment;
import com.newProject.ws.model.Player;
import com.newProject.ws.repository.CommentRepository;

@Service
public class CommentService {
	
	CommentRepository commentRepository;
	
	PlayerService playerService;
	
	public CommentService(CommentRepository commentRepository, PlayerService playerService) {
		super();
		this.commentRepository = commentRepository;
		this.playerService = playerService;
	}
	
	public void save(Comment comment, Player player) {
		comment.setTimeStamp(new Date());
		comment.setPlayer(player);
		commentRepository.save(comment);
	}
	
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
