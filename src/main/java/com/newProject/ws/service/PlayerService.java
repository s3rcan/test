package com.newProject.ws.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.newProject.ws.model.Player;
import com.newProject.ws.model.vm.PlayerUpdateVM;
import com.newProject.ws.repository.PlayerRepository;

@Service
public class PlayerService {
	

	PlayerRepository playerRepository;

	public PlayerService(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}
	
	@PostConstruct
	void init() {
		List<Player> list = this.playerRepository.findAll();
		for (Player player : list) {
			System.out.println(player);
			}
	}
		
	public void save(Player player) {
		playerRepository.save(player);
	}
	
	public void deletePlayer(Integer id) {
		playerRepository.deleteById(id);
	}
	
	public Player getByFirstName(String firstName) {
		Player inDB = playerRepository.findByFirstName(firstName);
		return inDB;
	}
	
//	public UserDetail getByEmail(String firstName???) {
//		UserDetail inDB = userRepository.findByEmail(email);
//		return inDB;
//	}
	
	
	public Player updatePlayer(String firstName, PlayerUpdateVM updatedPlayer) {
		Player inDB = getByFirstName(firstName);
		inDB.setFirstName(updatedPlayer.getFirstName());
		inDB.setEmail(updatedPlayer.getEmail());
		
		return playerRepository.save(inDB);
	}
	
	
	public Player findEmail(String email) {
		Player inDB = playerRepository.findByEmail(email);
		
		return inDB;
		
	}



}
