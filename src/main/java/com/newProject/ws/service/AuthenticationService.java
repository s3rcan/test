package com.newProject.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.newProject.ws.model.User;
import com.newProject.ws.security.UserPrincipal;
import com.newProject.ws.security.jwt.IJwtProvider;

//Oturum açma operasyonları(sign-in, sign-up ) 7

@Service
public class AuthenticationService 
{	
	@Autowired
	private AuthenticationManager authenticationManager;   //kullanıcı ve ss arasındaki köprüyü sağlar. Arka tarafta AM'ın yaptığı iş UserDetailsService'ten (CustomUserDet) loadUserByUsername yöntemini çağırmaktır
															// dolayısıyla authentication nesnemiz userPrincipal nesnesini içerir.
	@Autowired
	private IJwtProvider jwtProvider;
	
	public String signInAndReturnJWT(User signInRequest)		//authentication nesnesi.
	{
		Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword())
				);
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal(); //userPrincipal nesnesine ulaşmak için authentication.getprincipal(); yöntemini kullanabiliriz.
		
		return jwtProvider.generateToken(userPrincipal);			//son olarak jwt'yi userPrincipal nesnesi kullanarak döndüreceğiz. JwtProvider'da yaptığımız için direkt inject vur geç.
	}
}
