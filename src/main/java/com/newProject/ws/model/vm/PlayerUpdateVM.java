package com.newProject.ws.model.vm;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PlayerUpdateVM {
	
	@Column
	private String firstName;
	
	@NotNull
	@Column
	private String email;

}
