package com.newProject.ws.model.vm;

import javax.persistence.Column;

import com.newProject.ws.model.Player;

import lombok.Data;

@Data
public class PlayerVM {
	
	@Column
	private String firstName;
	
	@Column
	private String lastName;
	
	@Column
	private String email;
	
	public PlayerVM(Player player) {
		this.setFirstName(player.getFirstName());
		this.setLastName(player.getLastName());
		this.setEmail(player.getEmail());
	}
}
