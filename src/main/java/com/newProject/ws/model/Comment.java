package com.newProject.ws.model;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


import lombok.Data;

@Data
@Entity
@Table(name = "Comment")
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Size(min=1, max=255)
	@Column
	private String content;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeStamp;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "playerId")
	private Player player;
}