package com.newProject.ws.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.newProject.ws.security.jwt.IJwtProvider;

//Client'tan gelen istekleri yerine getirmek için veya filtrelemek için 6

public class JwtAuthorizationFilter extends OncePerRequestFilter{				//OncePerReq. kimliği doğrulanmış kişiyi almak için ss'in özel bir filtresi. 
	
	@Autowired									//Bu filtre bir spring nesnesi değil gidip SecurityConfig içinde @Bean oluştur. JwtAuthorizationFilter @Autowired için yani
	private IJwtProvider jwtProvider;  			//authentication nesnesi oluşturuyo zaten. inject et geç. 
	

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException 
	{
		Authentication authentication = jwtProvider.getAuthentication(request);
		
		if (authentication != null && jwtProvider.validateToken(request))
		{
			SecurityContextHolder.getContext().setAuthentication(authentication);		//SecurityContextHolder thread bazlı bir container ya da oturum nesnesi olarak düşünebilirmişiz
		}
		
		filterChain.doFilter(request, response);
	}

}

//son olarak bu filtreyi webSecurityConfig'den (SecurityConfig)tanımlamamız gerek. Çünkü bunun bir security nesnesi olduğunu söylememiz ve handi sırada çağıralacağını belirtmemiz gerek
//filtre username and password authentication filter'dan önce ele alınmalıdır. Çünkü ss bu filtrede kullanıcı adı ve parola veya authentication nesnesi bekler