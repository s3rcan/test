package com.newProject.ws.security;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

//Spring security için yeni bir UserDetails nesnesi oluşturmaca 2

@Getter
@NoArgsConstructor			//lombok boş constructor sağlar
@AllArgsConstructor			//lombok tüm bağımsız değişkenler için constructor sağlar
public class UserPrincipal implements UserDetails {

	private Long id;
	private String username;
	transient private String password; //don't show up on serialized places(şifreyi görünmez yapma)
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {					//user rolü yaratma
		
		return Collections.singletonList(new SimpleGrantedAuthority("USER"));

	}

	@Override
	public String getPassword() {
		
		return password;
	}

	@Override
	public String getUsername() {
		
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return true;
	}

}
