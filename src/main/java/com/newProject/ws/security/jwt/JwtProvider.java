package com.newProject.ws.security.jwt;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.newProject.ws.security.UserPrincipal;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParserBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

//app properties'e jwt private and public key eklemece 4.
//5 JwtProvider kurmaca. Bu sınıfta üç tane util yöntemine ihtiyaç var. 1 generateToken, 2 getAuthentication, 3 validateToken

@Component
public class JwtProvider implements IJwtProvider{
	
	@Value("${authentication.jwt.expiration-in-ms}")   					//@Value ile application.properties'den jwt özelliklerini çağırmaca
	private Long JWT_EXPIRATION_IN_MS;
	
	private static final String JWT_TOKEN_PREFIX = "Bearer";
	private static final String JWT_HEADER_STRING = "Authorization";
	
	private final PrivateKey jwtPrivateKey;								//Dependencie injection yöntemi, constructor ile inject etme olacak. genel ve özel key özelliklerini tanımlayabiliriz
	private final PublicKey jwtPublicKey;
	
	public JwtProvider(@Value("${authentication.jwt.private-key}") String jwtPrivateKeyStr,
					   @Value("${authentication.jwt.public-key}") String jwtPublicKeyStr) 
	{
		KeyFactory keyFactory = getKeyFactory();				//base64 code çözücü. elimizde base64 formatında privatekey ve public text'i var bunları java'ya anlatmaca... javanın anlayacağı değere çevirme.
		
		try
		{
			Base64.Decoder decoder = Base64.getDecoder();
			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decoder.decode(jwtPrivateKeyStr.getBytes()));  //Base64 private key kodlayıcı
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(decoder.decode(jwtPublicKeyStr.getBytes()));		//Base64 public key kodlayıcı
			
			jwtPrivateKey = keyFactory.generatePrivate(privateKeySpec);
			jwtPublicKey = keyFactory.generatePublic(publicKeySpec);
		}
		catch (Exception e)
		{
			throw new RuntimeException("Invalid key specification", e);
		}
		
	}
	
	@Override
	public String generateToken(UserPrincipal authentication)								//kullanıcı adı, şifre vb gibi kullanıcı bilgileriyle token oluşturur. 
	{
		String authorities = authentication.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining());
		
		
		return Jwts.builder()
				.setSubject(authentication.getUsername())
				.claim("userId", authentication.getId())
				.claim("roles", authorities)
				.setExpiration(new Date(System.currentTimeMillis() + JWT_EXPIRATION_IN_MS))
				.signWith(SignatureAlgorithm.RS512, jwtPrivateKey)
				.compact();
	}
	
	
	@Override
	public Authentication getAuthentication(HttpServletRequest request)			//client'tan gelen isteği doğrulayan method. kimlik doğrulandıktan sonra her istekte yetkilendirme bağlığı gönderir. Bu başlık token içerir
	{
	 
		String token = resolveToken(request);
		if (token == null)
		{
			return null;
		}
		System.out.println(token);
		
		Claims claims = Jwts.parser()
				.setSigningKey(jwtPublicKey)	
				.parseClaimsJws(token)
				.getBody();
		
		String username = claims.getSubject();
		Long userId = claims.get("userId", Long.class);
		List<GrantedAuthority> authorities = Arrays.stream(claims.get("roles").toString().split(","))
				.map(role -> role.startsWith("ROLE_") ? role : "ROLE" + role)
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
		
		UserDetails userDetails = new UserPrincipal(userId, username, null);
		
		return username != null ? new UsernamePasswordAuthenticationToken(userDetails, null, authorities) : null;
				
	}
	
	@Override
	public boolean validateToken(HttpServletRequest request)			//token'ı doğrulamak için kullanılır. Süresi dolmuş mu vs
	{
		String token = resolveToken(request);
		if (token == null)
		{
			return false;
		}
		Claims claims = Jwts.parser()
				.setSigningKey(jwtPublicKey)
				.parseClaimsJws(token)
				.getBody();
		
		if (claims.getExpiration().before(new Date()))
		{
			return false;
		}
		return true;
	}
	
	
	private String resolveToken(HttpServletRequest request)				//validateToken için oluşturulan method
	{
		String bearerToken = request.getHeader(JWT_HEADER_STRING);
		if (bearerToken != null && bearerToken.startsWith(JWT_TOKEN_PREFIX))
		{
			return bearerToken.substring(7);
		}
		return null;
	}
	
	
	private KeyFactory getKeyFactory() 												//key factory bulmayı ayrı bir method olarak tanımladık. "RSA"
	{
		try 
		{
		return KeyFactory.getInstance("RSA");
		}
		catch (NoSuchAlgorithmException e) 
		{
			throw new RuntimeException("Unknown key generation algorithm", e);
		}
	}
	
	
//	private Object getAuthority(Object object1, Object object1) {
//		return null;
//	}
}
