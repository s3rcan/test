package com.newProject.ws.security.jwt;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

import com.newProject.ws.security.UserPrincipal;

public interface IJwtProvider {

	String generateToken(UserPrincipal authentication);

	boolean validateToken(HttpServletRequest request);

	Authentication getAuthentication(HttpServletRequest request);

}
