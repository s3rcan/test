package com.newProject.ws.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//Spring security, Authentication Manager'a ulaşmak için WebSecurityConfigurer Adapter sınıfını sağlar 3


@Configuration														//Bu sınıfta bazı beanler tanımlayacağız. Bu da Configuration anotasyonuyla sağlanır
@EnableWebSecurity 													// Spring security'ye, bu sınıfın bir web güvenliği yapılandırma sınıfı olduğunu söylüyoruz 
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	   
	@Value("${service.security.secure-key-username}")
    private String SECURE_KEY_USERNAME;

    @Value("${service.security.secure-key-password}")
    private String SECURE_KEY_PASSWORD;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {

        auth
        .userDetailsService(customUserDetailsService)
        .passwordEncoder(passwordEncoder());
        
    }
    
    @Override
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
    	
    	return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable();																	//ss'nin api'lara erişim engelini açmak için
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);		//kimlik doğrulama ve oturumun ne kadar süreceğini belitir.
        
        http.authorizeHttpRequests()
        .antMatchers(HttpMethod.OPTIONS).permitAll()
        .antMatchers("/api/authentication/**").permitAll()										//login ve register pre-path
        .anyRequest().authenticated();
        
        http.addFilterBefore(jwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);  //filtre sırası!!!!
    }
    
    @Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
    
    @Bean
    public JwtAuthorizationFilter jwtAuthorizationFilter()
    {
    	return new JwtAuthorizationFilter();
    }
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
    	
    	return new WebMvcConfigurer() {
    		public void addCorsMappings(CorsRegistry registry) {
    			
    			registry.addMapping("/**")
    			.allowedOrigins("*")
    			.allowedMethods("*");
    		}
		};
    }
    
}
