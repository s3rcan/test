package com.newProject.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.newProject.ws.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);
	
	@Query(nativeQuery = true, value="select d from users d")
	List<User []> getAll();

	@Query(nativeQuery = true, value="select d.id, d.username from users d")
	List<Object []> getAll2();
	
//	@Query("select d from users u where u.email = ?1")
//	User findByEmailAddress(String email);
	
	@Query("From User where username=:username ")
	List<User> findByName(String username);
}
