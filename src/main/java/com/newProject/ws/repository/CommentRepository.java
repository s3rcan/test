package com.newProject.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.newProject.ws.model.Comment;
import com.newProject.ws.model.Player;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
		
	Comment findByPlayer(Player player);
}
