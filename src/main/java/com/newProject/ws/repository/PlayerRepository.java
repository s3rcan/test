package com.newProject.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.newProject.ws.model.Player;


public interface PlayerRepository extends JpaRepository<Player, Integer>{

	Player findByFirstName(String firstName);
	Player findByEmail(String email);

	//JPQL buralar! select bilmem ne bilmem ne querry
	//querry anno native_query=true;

//	@Query("select email FROM User")
//	public List<User> getAllUser();
 

}
